apt update
apt-get -y install python3-pip
apt-get -y install apache2
apt-get -y install libapache2-mod-wsgi
pip3 install alembic
pip3 install Flask 
pip3 install Flask-Migrate 
pip3 install Flask-SQLAlchemy 
pip3 install Jinja2
pip3 install numpy 
pip3 install pandas 
pip3 install SQLAlchemy 
pip3 install sqlparse 
pip3 install uuid
pip3 install virtualenv 
pip3 install Werkzeug 
pip3 install waitress 

